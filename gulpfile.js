var gulp = require('gulp');
var less = require('gulp-less');
var notify = require('gulp-notify');
var bower = require('gulp-bower');
var path = require('path');
var gulpsync = require('gulp-sync')(gulp);

var LessPluginCleanCSS = require('less-plugin-clean-css'),
    LessPluginAutoPrefix = require('less-plugin-autoprefix'),
    cleancss = new LessPluginCleanCSS({ advanced: true }),
    autoprefix= new LessPluginAutoPrefix({ browsers: ["last 2 versions"] });

var public_base_dir = path.join('public', 'static');
var resources_dir = 'resources';

var config = {
  lessPath: path.join(resources_dir, 'less'),
  jsPath: path.join('resources', 'js'),
  bowerDir: './bower_components'
}

function copyJS(min) {
  return function() {
    var public_js = path.join(public_base_dir, 'js');

    gulp.src(path.join(config.bowerDir, 'bootstrap', 'dist', 'js', min?'bootstrap.min.js':'bootstrap.js'))
      .pipe(gulp.dest(public_js));
    gulp.src(path.join(config.bowerDir, 'jquery', 'dist', min?'jquery.min.js':'jquery.js'))
      .pipe(gulp.dest(public_js));

    gulp.src(path.join(config.jsPath, '**', '*.js'), {base: config.jsPath})
      .pipe(gulp.dest(public_js));
  };
}

function configCSS(min) {
  var plugins = [autoprefix];
  if (min) {
    plugins.push(cleancss);
  }

  return function () {
    return gulp.src(path.join(config.lessPath, '*.less'))
      .pipe(less({
        paths: [
          path.join('resources', 'less'),
          path.join(config.bowerDir, 'bootstrap', 'less'),
          path.join(config.bowerDir, 'font-awesome', 'less')
        ],
        plugins: plugins
      })
        .on('error', notify.onError(function (error) {
          return 'Error: ' + error.message;
        })))
      .pipe(gulp.dest(path.join(public_base_dir, 'css')));
  }
}

gulp.task('bower', function () {
  return bower()
    .pipe(gulp.dest(config.bowerDir));
});

gulp.task('icons', function () {
  return gulp.src(path.join(config.bowerDir, 'font-awesome', 'fonts', '**.*'))
    .pipe(gulp.dest(path.join(public_base_dir, 'fonts')));
});

gulp.task('css', configCSS(false));

gulp.task('js', copyJS(false));

gulp.task('watch', function () {
  gulp.watch(path.join(config.lessPath, '**', '*.less'), ['css']);
});

gulp.task('default', gulpsync.sync(['bower', 'icons', ['css', 'js']]));
